// ParsleyConfig definition if not already set
window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n || {};

window.ParsleyConfig.i18n.pt = $.extend(window.ParsleyConfig.i18n.pt || {}, {
  defaultMessage: "Este valor parece ser inválido.",
  type: {
    email:        "Este valor deve ser um e-mail válido.",
    url:          "Este valor deve ser uma URL válida.",
    number:       "Este valor deve ser um número válido.",
    integer:      "Este valor deve ser um número válido.",
    digits:       "Este valor deve ser um dígito válido.",
    alphanum:     "Este valor deve ser alfanumérico."
  },
  notblank:       "Este valor no deve estar en blanco.",
  required:       "Este valor é necessário.",
  pattern:        "Este valor es incorrecto.",
  min:            "Este valor no deve ser menor que %s.",
  max:            "Este valor no deve ser mayor que %s.",
  range:          "Este valor deve estar entre %s y %s.",
  minlength:      "Este valor es muy corto. La longitud mínima es de %s caracteres.",
  maxlength:      "Este valor é muito grande. A quantidade maxima é de %s caracteres.",
  length:         "La longitud de este valor deve estar entre %s y %s caracteres.",
  mincheck:       "Deve seleccionar al menos %s opciones.",
  maxcheck:       "Deve seleccionar %s opciones o menos.",
  rangecheck:     "Deve seleccionar entre %s y %s opciones.",
  equalto:        "Este valor deve ser idéntico."
});

// If file is loaded after Parsley main file, auto-load locale
if ('undefined' !== typeof window.ParsleyValidator)
  window.ParsleyValidator.addCatalog('pt', window.ParsleyConfig.i18n.pt, true);
