tinymce.init({
    selector: "textarea",    
    width: 500,
    height: 300,        
    toolbar: "insertfile undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullpage | forecolor backcolor",     
});