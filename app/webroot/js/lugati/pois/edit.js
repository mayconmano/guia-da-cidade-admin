$(document).ready(function(){

  $('#categorias').multiselect({
  	includeSelectAllOption: true,
  	enableCaseInsensitiveFiltering: true,
  	maxHeight: 300,    
    buttonWidth: '435px'
  });

  tinymce.init({
    selector: "textarea",    
    width: 500,
    height: 300,        
    toolbar: "insertfile undo redo | styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullpage | forecolor backcolor",     
  }); 

  App.wizard();

  window.ParsleyValidator.setLocale('pt');

  var item = 1;
  var notAllow = [];

  $('.idiomas:checked').each(function(){
    switch($(this).val())
    {
      case 'pt':
        notAllow.push(2);              
      break;
      case 'en':
        notAllow.push(3);              
      break;
      case 'es':
        notAllow.push(4);        
      break;
    }  
    notAllow.sort();    
  });

  $('#wizard1').on('change', function(e, data) {

    if(data.step == 1){
      var validate_idiomas = false;
      $('.idiomas:checked').each(function(){
        validate_idiomas = true;   
      });
      if(!validate_idiomas) e.preventDefault();
    }     

    if(notAllow.indexOf(data.step) !== -1)
    {
      if (false === $('#form_poi').parsley().validate('block' + data.step))
        e.preventDefault();  
    }    
          
    item = $('#wizard1').wizard('selectedItem').step;   
  });

  
  $('#wizard1').on('changed', function(e, data) {
    selectedItem = $('#wizard1').wizard('selectedItem');    
    if(notAllow.indexOf(selectedItem.step) == -1){
      if(item > selectedItem.step)
        $('#wizard1').wizard('previous');
      else
      {
        if(selectedItem.step != 5)
          $('#wizard1').wizard('next');
      }
    }

    if(selectedItem.step == 5)
    {
      $("#geocomplete").geocomplete({
        map: ".map_canvas",
        details: "form ",
        markerOptions: {
          draggable: true
        },
        detailsAttribute: "data-geo",
        location: [$("input[data-geo=lat]").val(), $("input[data-geo=lng]").val()],

      });
      
      $("#geocomplete").bind("geocode:dragged", function(event, latLng){      
        $("input[data-geo=lat]").val(latLng.lat());
        $("input[data-geo=lng]").val(latLng.lng());
      });
      
      
      $("#find").click(function(){
        $("#geocomplete").trigger("geocode");
      }).click();
    }

  });

  $('#wizard1').on('finished', function(e){    
    $('#form_poi').submit();
  });

  $('.idiomas').on('ifChecked', function(event){
    var val = $(this).val();
    $('.steps li').each(function(i,v){        
        if($(this).attr('idioma') == val) 
        {
          $(this).removeClass('hide'); 
          switch($(this).attr('idioma'))
          {
            case 'pt':
              notAllow.push(2);              
            break;
            case 'en':
              notAllow.push(3);              
            break;
            case 'es':
              notAllow.push(4);        
            break;
          }          
        }
      });  
      notAllow.sort();
  });

  $('.idiomas').on('ifUnchecked', function(event){
     var val = $(this).val();
     $('.steps li').each(function(i, v){        
        if($(this).attr('idioma') == val) 
        {
          $(this).addClass('hide'); 
          switch($(this).attr('idioma'))
          {
            case 'pt':
              index = notAllow.indexOf(2);
              notAllow.splice(index,1);  
            break;
            case 'en':
              index = notAllow.indexOf(3);
              notAllow.splice(index,1);
            break;
            case 'es':              
              index = notAllow.indexOf(4);
              notAllow.splice(index,1);
            break;
          }  
        }
      });
      notAllow.sort(); 
  });



  App.masks();
  $('input[data-mask="telefone"]').mask('(99) 9999-9999?9');    
  $('input[data-mask="moeda"]').maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

    $('.wizard-previous, .wizard-next').click(function(){
      $("html, body").animate({ scrollTop: 0 }, "slow");
    });

    $('#fileupload').fileupload({    
        url: config_cidade+'/poies/uploadHandlerEdit/'+$('input[name="id"]').val(),
        maxNumberOfFiles: 8
    });
    
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

   
    $('#fileupload').addClass('fileupload-processing');

    $.ajax({
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });



});

function rawurlencode(str) {

  str = (str + '').toString();

  return encodeURIComponent(str)
    .replace(/!/g, '%21')
    .replace(/'/g, '%27')
    .replace(/\(/g, '%28')
    .replace(/\)/g, '%29')
    .replace(/\*/g, '%2A');
}