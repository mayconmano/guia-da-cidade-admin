
<div class="row">
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Top 10</h3>
      </div>
      <div class="content">

      	<form class="center">
	      	
      		
      		<select id="countries" class="multiselect" multiple="multiple" name="countries[]" style="width: 600px; height: 200px;">        		
        		<?php 
		    		foreach ($tops as $key => $top) {
		    			echo '<option value="'.$top['p']['id'].'" selected>'.$top['p']['nome_pt'].'</option>';
		    		}
		    	?>
        		<?php 
		    		foreach ($poies as $key => $poie) {
		    			if(in_array($key, $tops) == false)
		    				echo '<option value="'.$key.'">'.$poie.'</option>';
		    		}
		    	?>
        	</select>
      			
      		
      		<br>

		    <button type="submit" class="btn btn-primary">Salvar</button>                  
		        
		</form>
      </div>
    </div>
    
  </div>
</div>