<div class="row">
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Telefones Úteis</h3>
      </div>
      <div class="content">
        <form class="form-horizontal group-border-dashed" action="" method="post">
                	
	   <div class="form-group">	
				<label class="col-sm-3 control-label">Nome do local em português</label>
				<div class="col-sm-6">
                  <input type="text" name="nome_local_pt" class="form-control" placeholder="Nome do local em português">
                </div>
			</div>

			<div class="form-group">	
				<label class="col-sm-3 control-label">Nome do local em inglês</label>
				<div class="col-sm-6">
                  <input type="text" name="nome_local_en" class="form-control" placeholder="Nome do local em inglês">
                </div>
			</div>

			<div class="form-group">	
				<label class="col-sm-3 control-label">Nome do local em espanhol</label>
				<div class="col-sm-6">
                  <input type="text" name="nome_local_es" class="form-control" placeholder="Nome do local em espanhol">
                </div>
			</div>

			<div class="form-group">	
				<label class="col-sm-3 control-label">Telefone</label>
				<div class="col-sm-6">
                  <input type="text" name="telefone" class="form-control" placeholder="telefone">
                </div>
			</div>

      <div class="form-group">
            <label class="col-sm-3 control-label">Situação</label>
            <div class="col-sm-6">
              <div class="radio">                
               <label class="radio-inline"> <input type="radio" name="status" class="icheck" value="1"> Ativo</label>
               <label class="radio-inline"> <input type="radio" name="status" class="icheck" value="0" checked> Inativo</label> 
              </div>                    
            </div>
          </div>   

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">Salvar</button>                  
            </div>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>