<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Contatos</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>								
								<th>Nome</th>										
								<th>Email</th>								
								<th>Assunto</th>
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php							
								foreach ($contacts as $key => $contact) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$contact['Contact']['id'].'" class="'.$class.'">';
										echo '<td>'.$contact['Contact']['nome'].'</td>';
										echo '<td>'.$contact['Contact']['email'].'</td>';
										echo '<td>'.$contact['Contact']['assunto'].'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
			</div>
		</div>				
	</div>
</div>