<div class="row"> 
    <div class="col-md-12">

        <div class="block-flat">
            <div class="header">							
                <h3>Logs</h3>        
            </div>
            <div class="content">
                <div class="table-responsive">
                    <table class="table table-bordered" id="datatable" >
                        <thead>
                            <tr>														
                                <th>Usuário</th>
                                <th>Tabela</th>
                                <th>Titulo</th>						
                                <th>Data</th>						
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($logs as $key => $log) {
                                if ($key % 2 == 0)
                                    $class = "old";
                                else
                                    $class = "even";
                                echo '<tr class="' . $class . '">';
                                echo '<td>' . $log['User']['nome'] . '</td>';
                                echo '<td>' . $log['Log']['nome'] . '</td>';
                                echo '<td>' . $log['Log']['acao'] . '</td>';
                                echo '<td>' . $log['Log']['criado'] . '</td>';
                                echo '</tr>';
                            }
                            ?>											
                        </tbody>
                    </table>							
                </div>
            </div>
        </div>

    </div>
</div>