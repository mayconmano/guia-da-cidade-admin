<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Operadoras telefonicas</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>														
								<th>Nome</th>		
								<th>DDD</th>
								<th>Situação</th>
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php							
								foreach ($mobileoperators as $key => $mobileoperator) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$mobileoperator['Mobileoperator']['id'].'" class="'.$class.'">';		
										echo '<td>'.$mobileoperator['Mobileoperator']['nome'].'</td>';
										echo '<td>'.$mobileoperator['Mobileoperator']['ddd'].'</td>';
										echo '<td>'.($mobileoperator['Mobileoperator']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>').'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
				<a href="<?php echo $config_cidade; ?>/operadoras/adicionar"><button class="btn btn-primary">Adicionar</button></a>
			</div>
		</div>				
	</div>
</div>