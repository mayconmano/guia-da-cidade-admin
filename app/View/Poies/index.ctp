<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>POI's</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>								
								<th>Nome</th>														
								<th>Telefone</th>								
								<th>Situação</th>								
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php							
								foreach ($poies as $key => $poie) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$poie['Poie']['id'].'" class="'.$class.'">';
										 	echo '<td>'.$poie['Poie']['nome_pt'].'</td>';
										echo '<td>'.$poie['Poie']['telefone'].'</td>';
										echo '<td>'.($poie['Poie']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>').'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
			</div>
		</div>				
	</div>
</div>