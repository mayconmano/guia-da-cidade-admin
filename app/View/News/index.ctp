<div class="row">
	<div class="col-md-12">
		<div class="block-flat">
			<div class="header">							
				<h3>Notícias</h3>
			</div>
			<div class="content">
				<div class="table-responsive">
					<table class="table table-bordered" id="datatable" >
						<thead>
							<tr>								
								<th>Titulo</th>										
								<th>Data de Publicação</th>								
								<th>Situação</th>
								<th>Opções</th>
							</tr>
						</thead>
						<tbody>
							<?php							
								foreach ($news as $key => $new) {
									if($key % 2 == 0) $class = "old"; else $class = "even";
									echo '<tr id="'.$new['News']['id'].'" class="'.$class.'">';
										echo '<td>'.$new['News']['titulo'].'</td>';
										echo '<td>'.$this->Time->format($new['News']['data_publicacao'], '%d/%m/%Y').'</td>';
										echo '<td>'.($new['News']['status'] == 1 ? '<span class="label label-success inativo" style="cursor: pointer;">Ativo</span>' : '<span class="label label-default ativo" style="cursor: pointer;">Inativo</span>').'</td>';
										echo '<td></td>';
									echo '</tr>';								
								}		
							?>											
						</tbody>
					</table>							
				</div>
			</div>
		</div>				
	</div>
</div>