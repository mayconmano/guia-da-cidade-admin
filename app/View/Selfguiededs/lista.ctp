<div class="row">
    <div class="col-md-12">
        <div class="block-flat">
            <div class="header">							
                <h3>Roteiro auto guiado</h3>
            </div>
            <div class="content">
                <div class="table-responsive">
                    <table class="table table-bordered" id="datatable" >
                        <thead>
                            <tr>								
                                <th>Ordem</th>								
                                <th>Poie</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php                            
                            foreach ($poies as $key => $poie) {
                                if ($key % 2 == 0)
                                    $class = "old";
                                else
                                    $class = "even";
                                echo '<tr id="' . $poie['Poie']['id'] . '" class="' . $class . '">';
                                echo '<td>' . $poie['Poie']['ordem'] . '</td>';
                                echo '<td>' . $poie['Poie']['nome_pt'] . '</td>';                                                                
                                echo '</tr>';
                            }
                            ?>											
                        </tbody>
                    </table>							
                </div>
            </div>
        </div>				
    </div>
</div>