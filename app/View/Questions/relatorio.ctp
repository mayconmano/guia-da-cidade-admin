<h3><?php echo $dados[0]['q']['pergunta_pt']; ?></h3>
<div class="row">
	<?php foreach ($dados as $key => $value) { ?>
		<div class="col-sm-4">
			<div class="block-flat">
				<div class="header">							
					<h3><?php echo $value['a']['resposta_pt']; ?></h3>
				</div>
				<div class="content text-center">
					<div class="epie-chart" data-barcolor="#4D90FD" data-trackcolor="#F3F3F3" data-percent="<?php echo $value['0']['porcentagem']; ?>"><span>0%</span></div>
				</div>
				<?php echo 'Quantidade de resposta(s): '.$value['0']['qtd_respondidas']; ?>
			</div>					
		</div>
	<?php } ?>
</div>
