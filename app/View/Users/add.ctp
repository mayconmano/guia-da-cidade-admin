<div class="row">
  <div class="col-md-12">
  
    <div class="block-flat">
      <div class="header">							
        <h3>Cadastrar Usuário</h3>
      </div>
      <div class="content">
        <form class="form-horizontal group-border-dashed" action="" method="post">

          <div class="form-group">
            <label class="col-sm-3 control-label">Nome</label>
            <div class="col-sm-6">
              <input type="text" name="nome" class="form-control" required placeholder="Nome" />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">E-mail</label>
            <div class="col-sm-6">
              <input type="email" name="email" class="form-control" parsley-type="email" required placeholder="Nome" />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Senha</label>
            <div class="col-sm-6">
              <input type="password" name="senha" class="form-control" placeholder="Senha" />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Matricula</label>
            <div class="col-sm-6">
              <input type="text" name="matricula" class="form-control" placeholder="Matricula" data-parsley-type="number" data-parsley-minlength="3" required />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Cargo</label>
            <div class="col-sm-6">
              <input type="text" name="cargo" class="form-control" required placeholder="Cargo" />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Telefone Fixo</label>
            <div class="col-sm-6">
              <input type="text" name="telefone_fixo" class="form-control" data-mask="telefone" placeholder="Telefone Fixo" />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Telefone Ramal</label>
            <div class="col-sm-6">
              <input type="text" name="telefone_ramal" class="form-control" data-mask="telefone" placeholder="Telefone Ramal" />
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Telefone Celular</label>
            <div class="col-sm-6">
              <input type="text" name="telefone_celular" class="form-control" data-mask="telefone" placeholder="Telefone Celular" />
            </div>
          </div> 


          <div class="form-group">
            <label class="col-sm-3 control-label">Admin</label>
            <div class="col-sm-6">
              <div class="radio">                
               <label> <input type="checkbox" name="is_admin" value="1" class="icheck"></label> 
              </div>                    
            </div>
          </div> 

          <div class="form-group">
            <label class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
              <label class="radio-inline"> <input type="radio" name="status" value="1" class="icheck" checked> Ativo</label> 
              <label class="radio-inline"> <input type="radio" name="status" value="0" class="icheck"> Inativo</label>
            </div>
          </div> 

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">Cadastrar</button>                  
            </div>
          </div>
        </form>
      </div>
    </div>
    
  </div>
</div>