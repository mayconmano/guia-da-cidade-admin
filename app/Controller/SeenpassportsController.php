<?php
App::uses('AppController', 'Controller');
/**
 * Seenpassports Controller
 *
 * @property Seenpassport $Seenpassport
 * @property PaginatorComponent $Paginator
 */
class SeenpassportsController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Seenpassport->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The seenpassports could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Seenpassport->recursive = 0;
			if(empty($this->Seenpassport->find('first')))
			{
				$seenpassports['Seenpassport']['id'] = '';
				$seenpassports['Seenpassport']['descricao_pt'] = '';
				$seenpassports['Seenpassport']['descricao_en'] = '';
				$seenpassports['Seenpassport']['descricao_es'] = '';
				$this->set('seenpassports', $seenpassports);
			}else{
				$this->set('seenpassports', $this->Seenpassport->find('first'));
			}			
		}
	}

}
