<?php
App::uses('AppController', 'Controller');
/**
 * Watertransports Controller
 *
 * @property Watertransport $Watertransport
 * @property PaginatorComponent $Paginator
 */
class WatertransportsController extends AppController {


	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Watertransport->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Watertransport could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Watertransport->recursive = 0;
			if(empty($this->Watertransport->find('first')))
			{
				$watertransports['Watertransport']['id'] = '';
				$watertransports['Watertransport']['descricao_pt'] = '';
				$watertransports['Watertransport']['descricao_en'] = '';
				$watertransports['Watertransport']['descricao_es'] = '';
				$this->set('watertransports', $watertransports);
			}else{
				$this->set('watertransports', $this->Watertransport->find('first'));
			}			
		}
	}

}