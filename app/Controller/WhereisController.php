<?php
App::uses('AppController', 'Controller');
/**
 * Whereis Controller
 *
 * @property Wherei $Wherei
 * @property PaginatorComponent $Paginator
 */
class WhereisController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Wherei->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Wherei could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Wherei->recursive = 0;
			if(empty($this->Wherei->find('first')))
			{
				$whereis['Wherei']['id'] = '';
				$whereis['Wherei']['descricao_pt'] = '';
				$whereis['Wherei']['descricao_en'] = '';
				$whereis['Wherei']['descricao_es'] = '';
				$this->set('whereis', $whereis);
			}else{
				$this->set('whereis', $this->Wherei->find('first'));
			}			
		}
	}

}