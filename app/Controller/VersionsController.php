<?php
App::uses('AppController', 'Controller');
/**
 * Versions Controller
 *
 * @property Version $Version
 * @property PaginatorComponent $Paginator
 */
class VersionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->css[] = '/js/jquery.datatables/bootstrap-adapter/css/datatables';

		$this->js[] = 'jquery.datatables/jquery.datatables.min';
		$this->js[] = 'jquery.datatables/bootstrap-adapter/js/datatables';
		$this->js[] = 'lugati/versions/index';

		$this->Version->recursive = 0;
		$this->set('versions', $this->Version->find('all'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoRender = false;
		$this->Version->create();
		$this->request->data['Version']['data'] = date('Y-m-d H:s:i');
		$versao = $this->Version->find('first', array('order' => array('versao' => 'DESC')));
		$this->request->data['Version']['versao'] = (empty($versao) ? 1 : ($versao['Version']['versao']+1) );
		if ($this->Version->save($this->request->data)) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('The version could not be saved. Please, try again.'));
		}
	
	}

	public function delete($id = null) {
		$this->Version->id = $id;
		if (!$this->Version->exists()) {
			throw new NotFoundException(__('Invalid version'));
		}
		
		if ($this->Version->delete()) {
			$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
		} else {
			$this->Session->setFlash(__('The version could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
