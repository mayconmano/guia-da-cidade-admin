<?php
App::uses('AppController', 'Controller');
/**
 * Feeds Controller
 *
 * @property Feed $Feed
 * @property PaginatorComponent $Paginator
 */
class FeedsController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Feed->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feeds could not be saved. Please, try again.'));
			}
		}else{
			
			$this->Feed->recursive = 0;
			if(empty($this->Feed->find('first')))
			{
				$feeds['Feed']['id'] = '';
				$feeds['Feed']['link'] = '';
				$this->set('feeds', $feeds);
			}else{
				$this->set('feeds', $this->Feed->find('first'));
			}			
		}
	}

}
