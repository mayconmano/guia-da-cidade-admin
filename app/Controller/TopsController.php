<?php
App::uses('AppController', 'Controller');
/**
 * Tops Controller
 *
 * @property Top $Top
 * @property PaginatorComponent $Paginator
 */
class TopsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->css[] = '/js/jquery.gritter/css/jquery.gritter';				
		$this->css[] = 'jquery-ui';		
		$this->css[] = 'ui.multiselect';		

		$this->js[] = 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js';		
		$this->js[] = 'jquery.gritter/js/jquery.gritter';				
		$this->js[] = 'jquery.scrollTo-min';
		$this->js[] = 'ui.multiselect';

		$this->js[] = 'lugati/tops/index';
	
		$tops = $this->Top->query('SELECT p.id, p.nome_pt FROM tops t INNER JOIN poies p ON p.id = t.poie_id ORDER BY t.id');
		
		$poies = $this->Top->Poie->find('list', array('fields' => array('id', 'nome_pt'), 'conditions' => array('status = 1')));
		$this->set(compact('poies', 'tops'));
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {

		$this->layout = false;
		$this->autoRender = false;
		
		if ($this->request->is('post')) {						
			if($this->Top->query('TRUNCATE tops;')){				
				$this->Top->create();
				if ($this->Top->saveAll(json_decode($this->request->data['dados'], true))) {
					echo json_encode('sucesso');
				} else {
					echo json_encode('erro');
				}
			}
		}		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Top->exists($id)) {
			throw new NotFoundException(__('Invalid top'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Top->save($this->request->data)) {
				$this->Session->setFlash(__('The top has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The top could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Top.' . $this->Top->primaryKey => $id));
			$this->request->data = $this->Top->find('first', $options);
		}
		$poies = $this->Top->Poie->find('list');
		$this->set(compact('poies'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Top->id = $id;
		if (!$this->Top->exists()) {
			throw new NotFoundException(__('Invalid top'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Top->delete()) {
			$this->Session->setFlash(__('The top has been deleted.'));
		} else {
			$this->Session->setFlash(__('The top could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
