<?php
App::uses('AppController', 'Controller');
/**
 * Bicycles Controller
 *
 * @property Bicycle $Bicycle
 * @property PaginatorComponent $Paginator
 */
class BicyclesController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Bicycle->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Bicycle could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Bicycle->recursive = 0;
			if(empty($this->Bicycle->find('first')))
			{
				$bicycles['Bicycle']['id'] = '';
				$bicycles['Bicycle']['descricao_pt'] = '';
				$bicycles['Bicycle']['descricao_en'] = '';
				$bicycles['Bicycle']['descricao_es'] = '';
				$this->set('bicycles', $bicycles);
			}else{
				$this->set('bicycles', $this->Bicycle->find('first'));
			}			
		}
	}

}
