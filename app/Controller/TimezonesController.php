<?php
App::uses('AppController', 'Controller');
/**
 * Timezones Controller
 *
 * @property Timezone $Timezone
 * @property PaginatorComponent $Paginator
 */
class TimezonesController extends AppController {

	public $js = array();
	public $css = array();
	

	public function beforeRender(){				
		$this->set('js', $this->js);
		$this->set('css', $this->css);
	}

	public function index() {

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Timezone->save($this->request->data)) {
				$this->Session->setFlash(__('<div class="alert alert-success alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div class="icon"><i class="fa fa-check"></i></div><strong>Sucesso!</strong></div>'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The timezones could not be saved. Please, try again.'));
			}
		}else{
			$this->js[] = 'tinymce/js/tinymce/tinymce.min';	
			$this->js[] = 'lugati/editor-html';

			
			$this->Timezone->recursive = 0;
			if(empty($this->Timezone->find('first')))
			{
				$timezones['Timezone']['id'] = '';
				$timezones['Timezone']['descricao_pt'] = '';
				$timezones['Timezone']['descricao_en'] = '';
				$timezones['Timezone']['descricao_es'] = '';
				$this->set('timezones', $timezones);
			}else{
				$this->set('timezones', $this->Timezone->find('first'));
			}			
		}
	}

}
